# Titom

## How to setup

- Meteor

```bash
meteor npm install -g yarn
meteor yarn install
```

- MongoDB

[Installing](https://docs.mongodb.com/manual/installation/)

- Robo 3T

We use [Robo 3T](https://robomongo.org/) as a client for connection and handle mongo collections.

## How to run

-- localhost
```bash
meteor yarn start
```
-- using mongodb deployed
```bash
meteor yarn prod
```

Now, you can access the app at http://localhost:3000.

## How to format before commit

```bash
meteor yarn style
```

style is also called on pre-commit hooks.


### Know more

[Knowing our process](private/docs/)