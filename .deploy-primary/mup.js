module.exports = {
  servers: {
    one: {
      host: '167.71.88.99',
      username: 'root',
      pem: '/Users/tgmarinho/.ssh/id_rsa',
    },
  },
  meteor: {
    name: 'titom',
    path: '/Users/tgmarinho/Developer/codeftw/titom',
    docker: {
      image: 'zodern/meteor:root',
    },
    servers: {
      one: {},
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      PORT: 3000,
      MONGO_URL:
        'mongodb+srv://titomdb:m375taespPiad9NQ@cluster0-7wsai.mongodb.net/test?retryWrites=true',
      ROOT_URL: 'https://titomapp.com',
    },
    deployCheckWaitTime: 60,
  },
};
