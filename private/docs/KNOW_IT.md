# KNOW IT

# Development Process

* We communicate mainly by [Slack](https://codeftw.slack.com), here is how we use each channel:
  * titom: our main dev channel, you need to read everything that is written there. Also we have Bots messages, a lot of them.
  * help: ask for help here, the idea is to have immediate response in this channel
* We use English or Portuguese to communicate
* We use Kanban like app called [Trello](https://trello.com/b/BisXKV61/titom) to handle our tasks.
* Remember to move to `In Progress` the item on Trello before really start doing it. All the items `In Progess` must be really in progress currently. In progress means I'm online and working on this right now.
* When you stop remember to move to `Paused` or to `Pending Code Review` if you finished. Always re-read the card before move to `Pending Code Review`
  * is also important to do a self code review, try to do it as if the code had been written by someone else.
  * everybody should do Code Review, if we have items on `Pending Code Review` you should do it
    * If everything is as expected move the card to `Testing (Business)`
    * If anything is not as expected move the card to `Paused` and add the problems found as items on Checklist inside the Trello card or just comment that the problems are on Gitlab comments.
* You should include the Trello #Number in your commits message

  * The commit message needs to contain:
    * Trello Card Number
    * Trello Title
    * Details (optional only for small cards that will have only 1 commit)
      * Ex:
      ```
        #12 - Integração com Twitter
        - using Twet API for get endopoints and passport-twitter for authenticate
      ```
      * If you are using WebStorm that can be automatic, configure Trello on Tools > Tasks & Contexts > Configure Servers and follow the steps. Then go to Commit Message tab, enable it and paste this pattern: `#{number} - {summary}`. Now before starting a new Task just Switch to it using WebStorm tasks and the commit message will be added for you automatically. Remember, you still need to write the details of your commit.
  * If you don't have a card on Trello for what you are doing, you need to open it before commit, actually you should have opened before starting. Everything should be on Trello, everything.

## Open a bug

* Use the label `Bug` on Trello
* Attach a video showing the bug [Use Monosnap to record and upload the video](https://www.monosnap.com/welcome)

## Accept a card

* Every card on `Testing (Business)` is waiting an approval from a business stand point.
  * If everything is as expected move the card to `Done`
  * If anything is not as expected move the card to `Paused` and add the problems found as items on Checklist inside the Trello card.

# Code

## Specific imports

We need to import everything from a specific module because Meteor does not support tree shaking and then it will not eliminate code that is not used but is imported resulting in a larger client bundle size.

See below examples on how to do it.

### Exporting with withStyles if you need it
```
export const MyComponent = withStyles(styles)(
  ({
    propA,
	propB,
	propC,  
  }) => {
    useTitle('Bem-vindo!');

 
    return (
      <div className={classes.container}>
       
      </div>
    );
  }
);
```

### Importing

```
import { MyComponent } from './this/folder/mycomponent.js'
```

### Material UI Icons

Correct:

```javascript
import { default as Help } from '@material-ui/icons/Help';
```

Wrong:

```javascript
 import { Help } from '@material-ui/icons';
```


## How to format before commit

```bash
meteor yarn style
```

Style is also called on pre-commit hooks.



## Contribute

If you know other patterns that we follow, please write it in here! Help us keep all organized.