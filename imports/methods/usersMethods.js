import { Meteor } from 'meteor/meteor';

Meteor.methods({
  async getProfile() {
    const profile = await Meteor.users.findOne(Meteor.userId());

    const result = {
      _id: profile._id,
      name: profile.profile.name,
      login: profile.services.twitter.screenName,
      avatar: profile.services.twitter.profile_image_url_https,
    };

    return result;
  },
});
