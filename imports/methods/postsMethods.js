import { Meteor } from 'meteor/meteor';
import { PostsCollection } from '../collection/PostsCollection';
import { Users } from '../collection/UsersCollection';
import { getObject } from '../helpers/postsHelpers';
import { getTweetsByHomeTimeline } from '../api/TwitterIntegration';
import { WordsCollection } from '../collection/WordsCollection';
import { methodCall } from '../ui/methods';
import { rankPost, denyPost } from '../api/ranking';

const OPTIONS = { sort: { created_at: -1 }, limit: 400 };

Meteor.methods({
  async countPosts() {
    const count = await PostsCollection.find({
      userId: Meteor.userId(),
    }).count();
    return count;
  },
  deleteOldPosts() {
    /**
     * TODO: Pegar todos que não são bookmark
     */
    const referenceDate = new Date();
    referenceDate.setMonth(referenceDate.getMonth() - 1);
    PostsCollection.remove({ storedAt: { $lt: referenceDate } });
  },
  getNewTweetsAndSave() {
    return methodCall('tweetSave');
  },
  async tweetSave() {
    // vai ter q passar o token e secret de outra forma, dessa forma ta muito acoplado ao usuario logado
    const { accessToken, accessTokenSecret } = Meteor.user().services.twitter;

    const tweetsFetched = await getTweetsByHomeTimeline(
      accessToken,
      accessTokenSecret
    );

    await savePosts(tweetsFetched, Meteor.userId());

    return methodCall('findTweets', 0);
  },

  lastPostReceived(created_at) {
    const count = PostsCollection.find({
      userId: Meteor.userId(),
      created_at: { $gt: created_at },
    }).count();

    return count;
  },
  async getTweetAndSaveForAllUsers() {
    const users = await Users.find({}).fetch();

    for (const user of users) {
      const { accessToken, accessTokenSecret } = user.services.twitter;

      const tweetsFetched = await getTweetsByHomeTimeline(
        accessToken,
        accessTokenSecret
      );

      await savePosts(tweetsFetched, user._id);
    }
  },

  saveBookmark(tweet, bookmark) {
    PostsCollection.update(
      { userId: Meteor.userId(), _id: tweet._id },
      { $set: { bookmark } }
    );
  },
  async deleteTweet(id) {
    PostsCollection.update(
      { userId: Meteor.userId(), _id: tweet._id },
      { $set: { removed: true } }
    );
  },
  async findTweets(skip) {
    const limit = 5;
    const PARAMS = { sort: { ranking: -1, created_at: -1 }, skip, limit };
    return PostsCollection.find(
      { userId: Meteor.userId(), removed: false },
      PARAMS
    ).fetch();
  },
  findBookmarks() {
    const PARAMS = { sort: { ranking: -1, created_at: -1 } };
    return PostsCollection.find(
      { userId: Meteor.userId(), bookmark: true, removed: false },
      PARAMS
    ).fetch();
  },
});

async function savePosts(posts, userId) {
  const deniedWords = await WordsCollection.findDeniedWords(userId).fetch();

  const relevantWords = await WordsCollection.findRelevantWords(userId).fetch();

  for (const rawPost of posts) {
    const post = getObject(rawPost, userId);

    // se no tweet tiver alguma palavara proibida nao salvo no bd
    if (denyPost(post, deniedWords)) {
      continue;
    }

    const count = await PostsCollection.find({
      text: post.text,
      screen_name: post.screen_name,
      userId,
    }).count();

    // não permite salvar tweets duplicados
    if (count > 0) {
      continue;
    }
    // generates ranking
    post.ranking = rankPost(post, relevantWords);

    await PostsCollection.insert(post);
  }
}
