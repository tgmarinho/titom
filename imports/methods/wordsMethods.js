import { Meteor } from 'meteor/meteor';
import { WordsCollection } from '../collection/WordsCollection';
import { PostsCollection } from '../collection/PostsCollection';
import { rankPost } from '../api/ranking';

Meteor.methods({
  findDeniedWords() {
    return WordsCollection.findDeniedWords(Meteor.userId()).fetch();
  },
  addDeniedWord(word) {
    return WordsCollection.addDeniedWord(Meteor.userId(), word);
  },
  removeDeniedWord(word) {
    return WordsCollection.removeDeniedWord(Meteor.userId(), word);
  },
  removeAllDeniedWords() {
    return WordsCollection.removeAllDeniedWords(Meteor.userId());
  },

  findRelevantWords() {
    return WordsCollection.findRelevantWords(Meteor.userId()).fetch();
  },
  addRelevantWord(word) {
    const newWord = WordsCollection.addRelevantWord(Meteor.userId(), word);

    const wordRelevantsUpdated = WordsCollection.findRelevantWords(
      Meteor.userId()
    ).fetch();

    const posts = PostsCollection.find({ userId: Meteor.userId() }).fetch();

    for (const post of posts) {
      const ranking = rankPost(post, wordRelevantsUpdated);
      PostsCollection.update(post._id, { $set: { ranking } });
    }

    return newWord;
  },
  removeRelevantWord(word) {
    const relevants = WordsCollection.removeRelevantWord(Meteor.userId(), word);

    const wordRelevantsUpdated = WordsCollection.findRelevantWords(
      Meteor.userId()
    ).fetch();

    const posts = PostsCollection.find({ userId: Meteor.userId() }).fetch();

    for (const post of posts) {
      const ranking = rankPost(post, wordRelevantsUpdated);
      PostsCollection.update(post._id, { $set: { ranking } });
    }

    return relevants;
  },
  removeAllRelevantWords() {
    PostsCollection.update(
      { userId: Meteor.userId() },
      { $set: { ranking: 0 } },
      { multi: true }
    );

    return WordsCollection.removeAllRelevantWords(Meteor.userId());
  },
});
