import { SyncedCron } from 'meteor/percolate:synced-cron';
import { methodCall } from '../ui/methods';

SyncedCron.config({
  // Log job run details to console
  log: true, // Meteor.settings.public.env === 'DEVELOPMENT',
});

SyncedCron.add({
  name: 'Get new Tweets and save them',
  schedule(parser) {
    // parser is a later.parse object
    return parser.text('every 1 minutes'); // 16 minutes caso tenha expirado o Rate limit excceded
  },
  async job() {
    return methodCall('getTweetAndSaveForAllUsers');
  },
});

SyncedCron.add({
  name: 'Delete old posts',
  schedule(parser) {
    return parser.text('at 3:00');
  },
  async job() {
    return methodCall('deleteOldPosts');
  },
});
SyncedCron.start();
