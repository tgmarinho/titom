export const getObject = (
  {
    id,
    id_str,
    text,
    user: { name, screen_name, profile_image_url },
    entities: { media },
    created_at,
    favorite_count,
    retweet_count,
  },
  userId
) => ({
  id,
  id_str,
  text,
  name,
  screen_name,
  media_url: media ? media[0].media_url : null,
  profile_image_url,
  created_at: new Date(created_at),
  favorite_count,
  retweet_count,
  bookmark: false,
  ranking: 1,
  source: 'TWITTER',
  removed: false,
  userId,
});
