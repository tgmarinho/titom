import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const WordsCollection = new Mongo.Collection('words');

const max_weight = 10;

const wordsSchema = new SimpleSchema({
  _id: {
    type: String,
  },
  type: {
    type: String,
  },
  word: {
    type: String,
  },
  weight: {
    type: SimpleSchema.Integer,
  },
  userId: {
    type: String,
  },
});

WordsCollection.attachSchema(wordsSchema);

Object.assign(WordsCollection, {
  type: {
    RELEVANT: 'RELEVANT',
    DENIED: 'DENIED',
  },

  // DENIED
  findDeniedWords(userId) {
    return this.find(
      { userId, type: WordsCollection.type.DENIED },
      { _id: 0, word: 1 }
    );
  },
  addDeniedWord(userId, word) {
    word = polish(word);
    return this.insert({
      userId,
      type: WordsCollection.type.DENIED,
      word,
      weight: 0,
    });
  },
  removeDeniedWord(userId, word) {
    return this.remove({
      userId,
      type: WordsCollection.type.DENIED,
      word: word.word,
    });
  },
  removeAllDeniedWords(userId) {
    return this.remove({ userId, type: WordsCollection.type.DENIED });
  },

  // RELEVANT
  findRelevantWords(userId) {
    return this.find({ userId, type: WordsCollection.type.RELEVANT });
  },
  findByRelevantWord(userId, word) {
    return this.find({ userId, type: WordsCollection.type.RELEVANT, word });
  },
  addRelevantWord(userId, word) {
    word = polish(word);

    const relevantWords = this.findByRelevantWord(
      Meteor.userId(),
      word
    ).fetch();

    if (!relevantWords.length) {
      return this.insert({
        userId,
        type: WordsCollection.type.RELEVANT,
        word,
        weight: 1,
      });
    }

    const relevantWord = relevantWords[0];

    if (relevantWord.weight < max_weight) {
      return this.update({ _id: relevantWord._id }, { $inc: { weight: 1 } });
    }

    return relevantWord;
  },
  removeRelevantWord(userId, word) {
    return this.remove({
      userId,
      type: WordsCollection.type.RELEVANT,
      word: word.word,
    });
  },
  removeAllRelevantWords(userId) {
    return this.remove({ userId, type: WordsCollection.type.RELEVANT });
  },
});

const polish = word => word.toLowerCase().trim();
