import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const PostsCollection = new Mongo.Collection('posts');

const postsSchema = new SimpleSchema({
  _id: {
    type: String,
  },
  id: {
    type: String,
  },
  id_str: {
    type: String,
  },
  text: {
    type: String,
  },
  name: {
    type: String,
  },
  screen_name: {
    type: String,
  },
  media_url: {
    type: String,
    optional: true,
  },
  profile_image_url: {
    type: String,
  },
  created_at: {
    type: Date,
  },
  favorite_count: {
    type: SimpleSchema.Integer,
  },
  retweet_count: {
    type: SimpleSchema.Integer,
  },
  bookmark: {
    type: Boolean,
  },
  ranking: {
    type: SimpleSchema.Integer,
  },
  userId: {
    type: String,
  },
  source: {
    type: String,
  },
  removed: {
    type: Boolean,
  },
});

PostsCollection.attachSchema(postsSchema);
