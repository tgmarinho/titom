import { Meteor } from 'meteor/meteor';

const { users } = Meteor;

Object.assign(users, {});

export { users as Users };
