import { Enum } from 'enumify';

class EnumExtended extends Enum {
  static names() {
    return this.enumValues.map(x => x.name);
  }
  static enumValueOf(value) {
    return super.enumValueOf(value);
  }
  static initEnum(enumName, val) {
    this.enumName = enumName;
    super.initEnum(val);
  }
  static graphqlEnumDefinition() {
    return `enum ${this.enumName} {
              ${this.names().join('\n')}
            }`;
  }
}
export { EnumExtended };
