import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Redirect, Route } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      Meteor.userId() ? <Component {...props} /> : <Redirect to="/login" />
    }
  />
);
