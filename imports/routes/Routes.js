import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { withStyles } from '@material-ui/core';

import { Timeline } from '../ui/pages/Timeline';
import Words from '../ui/pages/Words/Words';
import { Login } from '../ui/pages/Login';
import { PrivateRoute } from './PrivateRoute';
import { RoutePaths } from './RoutePaths';
import { WordsEnum } from '../ui/pages/Words/WordEnum';
import { Bookmarks } from '../ui/pages/Bookmarks';

const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '8px',
    marginTop: '80px',
  },
});

export const Routes = withStyles(styles)(({ classes }) => (
  <Switch>
    <>
      <Route path={`/${RoutePaths.LOGIN}`} component={Login} />
      <div className={classes.container}>
        <PrivateRoute path={`/${RoutePaths.ROOT}`} component={Timeline} />
        <PrivateRoute path={`/${RoutePaths.TIMELINE}`} component={Timeline} />
        <PrivateRoute
          path={`/${RoutePaths.DENIED_WORDS}`}
          component={props => <Words {...props} WordEnum={WordsEnum.DENIED} />}
        />
        <PrivateRoute
          path={`/${RoutePaths.RELEVANT_WORDS}`}
          component={props => (
            <Words {...props} WordEnum={WordsEnum.RELEVANT} />
          )}
        />
        <PrivateRoute
          path={`/${RoutePaths.BOOKMARKS}`}
          component={props => <Bookmarks {...props} />}
        />
      </div>
    </>
  </Switch>
));
