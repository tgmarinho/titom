export const RoutePaths = {
  ROOT: '/',
  TIMELINE: 'timeline',
  LOGIN: 'login',
  RELEVANT_WORDS: 'relevantWords',
  DENIED_WORDS: 'deniedWords',
  BOOKMARKS: 'bookmarks',
  SOCIAL_NETWORKS: 'socialNetWorks',
};
