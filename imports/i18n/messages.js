export default {
  'pt-BR': {
    index_welcome: 'Bem vindo',
    index_slogan: 'Veja apenas o que é relevante do seu Twitter',
    index_login: 'Entrar',
    index_seeWhat: 'Veja o que te interessa primeiro',
    index_timeFirst: 'Não perca tempo com o que não é importante',
    index_followRelevant: 'Siga apenas o que é relevante',

    menu_timeline: 'Timeline',
    menu_relevantWords: 'Palavras Relevantes',
    menu_denyWords: 'Palavras Bloqueadas',
    menu_bookmark: 'Items Salvos',
    menu_logout: 'Sair',

    card_relevant: '{word} agora é relevante!',
    card_tooltip: '+ 1 toque para salvar a palavra {word}',
    card_copy_text: 'Copiado, só colar!',
    card_bookmarked: 'Tweet foi salvo...',
    card_deleted: 'Deletado...',

    timeline_all_checked: 'Opa, você já viu todos :)',
  },

  pt: {
    index_welcome: 'Bem vindo',
    index_slogan: 'Veja apenas o que é relevante do seu Twitter',
    index_login: 'Entrar',
    index_seeWhat: 'Veja o que te interessa primeiro',
    index_timeFirst: 'Não perca tempo com o que não é importante',
    index_followRelevant: 'Siga apenas o que é relevante',

    menu_timeline: 'Timeline',
    menu_relevantWords: 'Palavras Relevantes',
    menu_denyWords: 'Palavras Bloqueadas',
    menu_bookmark: 'Salvos',
    menu_logout: 'Sair',

    card_relevant: '{word} agora é relevante!',
    card_tooltip: '+ 1 toque para salvar a palavra {word}',
    card_copy_text: 'Copiado, só colar!',
    card_bookmarked: 'Tweet foi salvo...',
    card_deleted: 'Deletado...',

    timeline_all_checked: 'Opa, você já viu todos :)',
  },

  'en-US': {
    index_welcome: 'Welcome',
    index_slogan: 'See what is relevant to Twitter',
    index_login: 'Login',
    index_seeWhat: 'See what interests you first',
    index_timeFirst: 'Do not waste time on what is not important',
    index_followRelevant: 'Follow only what is relevant',

    menu_timeline: 'Timeline',
    menu_relevantWords: 'Relevant Words',
    menu_denyWords: 'Blocked Words',
    menu_bookmark: 'Bookmarks',
    menu_logout: 'Logout',

    card_relevant: '{word} is relant for you!',
    card_tooltip: '+ 1 touch to save this word {word}',
    card_copy_text: 'Copied to clipboard',
    card_bookmarked: 'Bookmarked...',
    card_unbookmarked: 'Unbookmarked...',
    card_deleted: 'Deleted...',

    timeline_all_checked: 'Hey, Have you seen all :)',
  },

  en: {
    index_welcome: 'Welcome',
    index_slogan: 'See what is relevant to Twitter',
    index_login: 'Login',
    index_seeWhat: 'See what interests you first',
    index_timeFirst: 'Do not waste time on what is not important',
    index_followRelevant: 'Follow only what is relevant',

    menu_timeline: 'Timeline',
    menu_relevantWords: 'Relevant Words',
    menu_denyWords: 'Blocked Words',
    menu_bookmark: 'Bookmarks',
    menu_logout: 'Logout',

    card_relevant: '{word} is relant for you!',
    card_tooltip: '+ 1 touch to save this word {word}',
    card_copy_text: 'Copied to clipboard',
    card_bookmarked: 'Bookmarked...',
    card_unbookmarked: 'Unbookmarked...',
    card_deleted: 'Deleted...',

    timeline_all_checked: 'Hey, Have you seen all :)',
  },
};
