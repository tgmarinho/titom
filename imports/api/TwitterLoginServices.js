import { Meteor } from 'meteor/meteor';

import { ServiceConfiguration } from 'meteor/service-configuration';

ServiceConfiguration.configurations.remove({
  service: Meteor.settings.twitter.SERVICE,
});

ServiceConfiguration.configurations.insert({
  service: Meteor.settings.twitter.SERVICE,
  loginStyle: Meteor.settings.twitter.LOGIN_STYLE,
  consumerKey: Meteor.settings.twitter.CONSUMER_KEY,
  secret: Meteor.settings.twitter.CONSUMER_SECRET,
});
