import { Meteor } from 'meteor/meteor';
import Twit from 'twit';

const HOME_TIMELINE = 'statuses/home_timeline';

export const getTweetsByHomeTimeline = (access_token, access_token_secret) => {
  const T = new Twit({
    consumer_key: Meteor.settings.twitter.CONSUMER_KEY,
    consumer_secret: Meteor.settings.twitter.CONSUMER_SECRET,
    access_token,
    access_token_secret,
    timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
    strictSSL: false, // optional - requires SSL certificates to be valid.
  });

  const PARAMS = { count: 5 };

  return new Promise((resolve, reject) => {
    T.get(HOME_TIMELINE, PARAMS, (error, data) => {
      if (error) reject(error);
      else resolve(data);
    });
  });
};
