function normalize(text) {
  return text.toLowerCase();
}

function countWord(text, word) {
  let count = -1;
  let pos = -1;

  do {
    count++;
    pos = text.split(' ').indexOf(word, ++pos);
  } while (pos != -1);

  return count;
}

function rankingByRelevantWordWeight(text, relevantWords) {
  let sum = 1;
  text = normalize(text);
  relevantWords.forEach(word => {
    sum += countWord(text, word.word) * word.weight;
  });

  return sum;
}

const rankPost = (post, relevantWords) =>
  rankingByRelevantWordWeight(post.text, relevantWords);

const denyPost = (post, deniedWords) => {
  text = normalize(post.text);
  for (word of deniedWords) {
    if (countWord(text, word)) {
      return true;
    }
  }
  return false;
};

export { rankPost, denyPost };
