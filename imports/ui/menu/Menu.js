import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { FormattedMessage } from 'react-intl';
import {
  withStyles,
  List,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
  Drawer,
  Avatar,
} from '@material-ui/core';
import { default as MenuIcon } from '@material-ui/icons/Menu';
import { default as TimelineIcon } from '@material-ui/icons/Timeline';
import { default as ThumbUpIcon } from '@material-ui/icons/ThumbUp';
import { default as ThumbDownIcon } from '@material-ui/icons/ThumbDown';
import { default as ExitToApp } from '@material-ui/icons/ExitToApp';
import { default as BookmarksIcon } from '@material-ui/icons/Bookmarks';
import { RoutePaths } from '../../routes/RoutePaths';

import { methodCall } from '../methods';

const styles = {
  link: {
    textDecoration: 'none',
  },
};

const useProfile = () => {
  const [profile, setProfile] = useState({});

  useEffect(() => {
    methodCall('getProfile').then(data => {
      setProfile({ ...data });
    });
  }, [profile._id]);

  return profile;
};

export const Menu = withStyles(styles)(({ classes, history }) => {
  const [toggleMenu, setToggleMenu] = useState(false);

  const profile = useProfile();

  const sideList = (
    <div className={classes.list}>
      <List>
        {/* <Link to={RoutePaths.ROOT} style={styles.link}> */}
        <ListItem button>
          <ListItemIcon>
            <Avatar alt={profile.name} src={profile.avatar} />
          </ListItemIcon>
          <ListItemText primary={profile.name} />
        </ListItem>
        {/* </Link> */}

        <Link to={RoutePaths.TIMELINE} style={styles.link}>
          <ListItem button>
            <ListItemIcon>
              <TimelineIcon />
            </ListItemIcon>
            <ListItemText primary={<FormattedMessage id="menu_timeline" />} />
          </ListItem>
        </Link>

        <Link to={RoutePaths.RELEVANT_WORDS} style={styles.link}>
          <ListItem button>
            <ListItemIcon>
              <ThumbUpIcon />
            </ListItemIcon>
            <ListItemText
              primary={<FormattedMessage id="menu_relevantWords" />}
            />
          </ListItem>
        </Link>
        <Link to={RoutePaths.DENIED_WORDS} style={styles.link}>
          <ListItem button>
            <ListItemIcon>
              <ThumbDownIcon />
            </ListItemIcon>
            <ListItemText primary={<FormattedMessage id="menu_denyWords" />} />
          </ListItem>
        </Link>
        <Link to={RoutePaths.BOOKMARKS} style={styles.link}>
          <ListItem button>
            <ListItemIcon>
              <BookmarksIcon />
            </ListItemIcon>
            <ListItemText primary={<FormattedMessage id="menu_bookmark" />} />
          </ListItem>
        </Link>
        <Divider />
        <ListItem
          button
          onClick={() =>
            Meteor.logout(() => {
              history.push(RoutePaths.LOGIN);
            })
          }
        >
          <ListItemIcon>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText primary={<FormattedMessage id="menu_logout" />} />
        </ListItem>
      </List>
    </div>
  );
  return (
    <Fragment>
      <IconButton
        className={classes.menuButton}
        color="inherit"
        aria-label="Menu"
        onClick={() => setToggleMenu(true)}
      >
        <MenuIcon />
      </IconButton>
      <Drawer open={toggleMenu} onClose={() => setToggleMenu(false)}>
        <div
          tabIndex={0}
          role="button"
          onClick={() => setToggleMenu(false)}
          onKeyDown={() => setToggleMenu(false)}
        >
          {sideList}
        </div>
      </Drawer>
    </Fragment>
  );
});
