import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { TunnelPlaceholder } from 'react-tunnels';
import {
  withStyles,
  AppBar,
  Toolbar,
  Typography,
  IconButton,
} from '@material-ui/core';
import { default as Timeline } from '@material-ui/icons/Timeline';

import { Menu } from './Menu';
import { RoutePaths } from '../../routes/RoutePaths';

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

export const AppTopBar = withStyles(styles)(
  withRouter(({ classes, history }) => (
    <div className={classes.root}>
      {Meteor.userId() ? (
        <AppBar>
          <Toolbar>
            <Menu history={history} />
            <Typography variant="h6" color="inherit" className={classes.grow}>
              <TunnelPlaceholder id="app-title" />
            </Typography>
            <IconButton
              color="inherit"
              aria-label="Timeline"
              onClick={() => history.push(RoutePaths.TIMELINE)}
            >
              <Timeline />
            </IconButton>
          </Toolbar>
        </AppBar>
      ) : (
        ''
      )}
    </div>
  ))
);
