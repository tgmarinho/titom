import React, { Fragment } from 'react';
import { CssBaseline } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';

import { Routes } from '../routes/Routes';
import { AppTopBar } from './menu/AppTopBar';

export const App = () => (
  <Fragment>
    <CssBaseline />
    <AppTopBar />
    <SnackbarProvider
      maxSnack={3}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
    >
      <Routes />
    </SnackbarProvider>
  </Fragment>
);
