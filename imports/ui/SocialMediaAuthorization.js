import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Typography, withStyles } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { TwitterLoginButton } from 'react-social-login-buttons';
import { RoutePaths } from '../routes/RoutePaths';

const styles = {
  socialButton: {
    width: '240px',
    height: '44px',
  },
  buttonTitle: {
    marginLeft: '50px',
    fontWeight: 'bold',
  },
};

export const SocialMediaAuthorization = withStyles(styles)(
  withRouter(({ classes, history }) => {
    const twitterLogin = async () => {
      await Meteor.loginWithTwitter(
        {
          requestPermissions: ['user', 'user_timeline'],
        },
        error => {
          if (error) {
            return error;
          }
          history.push(RoutePaths.ROOT);
        }
      );
    };

    return (
      <div>
        <Typography variant="h6" color="inherit" className={classes.flex}>
          Vincular suas redes sociais
        </Typography>
        <br />
        <TwitterLoginButton
          className={classes.socialButton}
          onClick={twitterLogin}
        >
          <span style={{ textAlign: 'center' }} className={classes.buttonTitle}>
            Twitter
          </span>
        </TwitterLoginButton>
      </div>
    );
  })
);
