import Validator from 'validator';

// TODO tem q colocar os textos em constantes e tbm deixar em portugues as msg para o usário
export const userValidation = user => {
  const errors = {};
  if (Validator.isEmpty(user.email)) {
    errors.email = 'Enter your email address';
  } else if (!Validator.isEmail(user.email)) {
    errors.email = 'Invalid email address';
  }
  if (!Validator.isLength(user.fullName, { min: 2, max: 50 })) {
    errors.fullName = 'Name must be between 2 and 50';
  }
  if (!Validator.isLength(user.password, { min: 6, max: 20 })) {
    errors.password = 'Password must be between 6 and 20';
  }
  if (Validator.isEmpty(user.password2)) {
    return errors;
  }
  if (Object.keys(errors).length !== 0) {
    return errors;
  }
  return false;
};
