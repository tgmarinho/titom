import React from 'react';
import { CardFeed } from './CardFeed';

export const Posts = ({ posts }) => (
  <div>
    {posts.map(post => (
      <CardFeed post={post} key={post._id} />
    ))}
  </div>
);
