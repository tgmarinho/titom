import React from 'react';
import { CircularProgress, Typography } from '@material-ui/core';

const style = {
  container: {
    position: 'relative',
  },
  refresh: {
    display: 'inline-block',
    position: 'relative',
  },
};
export const Loading = () => (
  <div className="loading-indicator">
    <CircularProgress size={50} style={style.refresh} />
  </div>
);

export const LoadingText = () => (
  <div>
    <Typography component="h5">Carregando</Typography>
  </div>
);
