import React, { Fragment, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { withSnackbar } from 'notistack';
import { FaTwitter } from 'react-icons/fa';
import { distanceInWords } from 'date-fns';
import pt from 'date-fns/locale/pt';
import {
  withStyles,
  Card,
  CardHeader,
  CardContent,
  CardMedia,
  CardActions,
  Avatar,
  IconButton,
  Typography,
  Tooltip,
  Link,
} from '@material-ui/core';
import { default as red } from '@material-ui/core/colors/red';
import { default as FileCopyIcon } from '@material-ui/icons/FileCopy';
import { default as DeleteIcon } from '@material-ui/icons/Delete';
import { default as BookmarkIconBorder } from '@material-ui/icons/BookmarkBorder';
import { default as BookmarkIcon } from '@material-ui/icons/Bookmark';

import { methodCall } from '../methods';

const styles = theme => ({
  card: {
    maxWidth: 800,
    margin: theme.spacing.unit * 2,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

const TooltipAction = withSnackbar(({ word, enqueueSnackbar }) => {
  const [open, setOpen] = useState(false);
  const [touched, setTouched] = useState(false);

  const handleTooltipOpen = () => {
    setOpen(true);
    setTimeout(() => setOpen(false), 2000);
  };

  const handleTooltipClose = () => {
    methodCall('addRelevantWord', word);
    enqueueSnackbar(
      <FormattedMessage id="card_relevant" values={{ word: <b>{word}</b> }} />,
      {
        variant: 'success',
      }
    );
    setOpen(false);
  };

  // Simula segundo tab no mobile
  const handleDoubleTap = () => {
    if (!touched) {
      setTouched(true);
    } else {
      methodCall('addRelevantWord', word);
      enqueueSnackbar(
        <FormattedMessage
          id="card_relevant"
          values={{ word: <b>{word}</b> }}
        />,
        {
          variant: 'success',
        }
      );
      setTouched(false);
    }
  };

  return (
    <Tooltip
      PopperProps={{
        disablePortal: false,
      }}
      open={open}
      disableFocusListener
      disableHoverListener
      disableTouchListener
      title={
        <FormattedMessage id="card_tooltip" values={{ word: <b>{word}</b> }} />
      }
    >
      <span
        onClick={handleTooltipOpen}
        onDoubleClick={handleTooltipClose}
        onTouchStart={handleDoubleTap}
      >
        {`${word} `}
      </span>
    </Tooltip>
  );
});

export const CardFeed = withSnackbar(
  withStyles(styles)(({ post, classes, enqueueSnackbar }) => {
    const [bookmark, setBookmark] = useState(post.bookmark || false);
    const [showCard, setShowCard] = useState(true);

    const handleBookmark = isBookmark => {
      methodCall('saveBookmark', post, !isBookmark);
      setBookmark(!bookmark);

      enqueueSnackbar(
        <FormattedMessage
          id={isBookmark ? 'card_unbookmarked' : 'card_bookmarked'}
        />,
        {
          variant: 'success',
        }
      );
    };

    // peguei da internet -> https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
    const copyText = text => {
      const el = document.createElement('textarea');
      el.value = text;
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      enqueueSnackbar(<FormattedMessage id="card_copy_text" />, {
        variant: 'info',
      });
    };

    const deleteTweet = async tweet => {
      await methodCall('deleteTweet', tweet._id);
      setShowCard(false);

      enqueueSnackbar(<FormattedMessage id="card_deleted" />, {
        variant: 'success',
      });
    };

    return (
      showCard && (
        <Card className={classes.card}>
          <CardHeader
            avatar={
              <Avatar
                aria-label="Avatar"
                className={classes.avatar}
                src={post.profile_image_url}
              />
            }
            // action={
            //   <IconButton>
            //     <MoreVertIcon />
            //   </IconButton>
            // }
            title={
              <Fragment>
                {post.name}{' '}
                <Link href={`https://twitter.com/@${post.screen_name}`}>
                  @{post.screen_name}
                </Link>
              </Fragment>
            }
            subheader={
              <span>
                há{' '}
                {distanceInWords(post.created_at, new Date(), {
                  locale: pt,
                })}
              </span>
            }
          />
          {!!post.media_url && (
            <a
              href={
                post.text.split(' ').filter(item => item.includes('http'))[0]
              }
            >
              <CardMedia
                className={classes.media}
                image={post.media_url}
                title={post.screen_name}
              />
            </a>
          )}
          <CardContent>
            {
              <Typography component="p">
                {post.text.split(' ').map((word, index) => (
                  <TooltipAction key={index} word={word} />
                ))}
              </Typography>
            }
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
            {!bookmark && (
              <IconButton
                aria-label="Excluir"
                onClick={() => deleteTweet(post)}
              >
                <DeleteIcon />
              </IconButton>
            )}
            <IconButton aria-label="Copiar" onClick={() => copyText(post.text)}>
              <FileCopyIcon />
            </IconButton>

            {bookmark ? (
              <IconButton
                aria-label="Bookmark"
                onClick={() => handleBookmark(bookmark)}
              >
                <BookmarkIcon />
              </IconButton>
            ) : (
              <IconButton
                aria-label="Bookmark"
                onClick={() => handleBookmark(bookmark)}
              >
                <BookmarkIconBorder />
              </IconButton>
            )}

            <Link
              href={`https://twitter.com/${post.screen_name}/status/${
                post.id_str
              }`}
            >
              <IconButton aria-label="Ler">
                <FaTwitter />
              </IconButton>
            </Link>
          </CardActions>
        </Card>
      )
    );
  })
);
