import React from 'react';
import { withStyles } from '@material-ui/core';

const styles = {
  containerLoadMore: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  buttonLoadingMore: {
    backgroundColor: '#145e43',
    color: 'white',
    borderRadius: '2rem',
    border: 'none',
  },
};

export const ButtonLoadMore = withStyles(styles)(
  ({ fetchMore, numDoc, totalDoc, classes }) => (
    <div className={classes.containerLoadMore}>
      <button
        onClick={fetchMore}
        className={classes.buttonLoadingMore}
      >{`Carregar mais (${numDoc}-${totalDoc})`}</button>
    </div>
  )
);
