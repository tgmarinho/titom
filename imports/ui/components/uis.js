import React from 'react';
import { Tunnel } from 'react-tunnels';
import messages from '../../i18n/messages';

export const updateAppTitle = title => {
  if (document) {
    let locale = window.navigator.language;
    if (!messages[locale]) {
      locale = 'en-US';
    }

    document.title = `${messages[locale][title.props.id]} | Titom` || 'Titom';
  }

  return <Tunnel id="app-title">{title || 'Titom'}</Tunnel>;
};
