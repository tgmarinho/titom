import React, { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { methodCall } from '../methods';
import { Posts } from '../components/Posts';
import { updateAppTitle } from '../components/uis';

export const Bookmarks = () => {
  const [content, setContent] = useState([]);

  // https://www.robinwieruch.de/react-hooks-fetch-data/
  useEffect(() => {
    let didCancel = false;

    async function fetchMyAPI() {
      const posts = await methodCall('findBookmarks');

      if (!didCancel) {
        // Ignore if we started fetching something else
        setContent(posts);
      }
    }

    fetchMyAPI();
    return () => {
      didCancel = true;
    }; // Remember if we start fetching something else
  }, []);

  // useEffect(() => {
  //   const posts = methodCall('findBookmarks');
  //   posts.then(data => setContent(data)).catch(err => console.error(err));
  // }, []);

  return (
    <>
      {updateAppTitle(<FormattedMessage id="menu_bookmark" />)}
      <Posts posts={content} />
    </>
  );
};
