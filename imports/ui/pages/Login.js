import React from 'react';
import { Meteor } from 'meteor/meteor';
import { TwitterLoginButton } from 'react-social-login-buttons';
import { withRouter, Redirect } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { withStyles, CardMedia, Typography } from '@material-ui/core';
import { default as Face } from '@material-ui/icons/Face';
import { default as HourglassEmpty } from '@material-ui/icons/HourglassEmpty';
import { default as FavoriteBorder } from '@material-ui/icons/FavoriteBorder';

import { RoutePaths } from '../../routes/RoutePaths';

const styles = {
  container: {
    background: '#fff',
    display: 'flex',
    flexDirection: 'column-reverse',
  },
  textPresention: {
    fontWeight: 'bold',
    fontSize: '2.4vh',
    color: '#FFF',
    marginLeft: '10px',
  },
  beginText: {
    fontWeight: 'bold',
    fontSize: '2.2vh',
    color: '#FFF',
  },
  topText: {
    fontWeight: 'bold',
    fontSize: '2.2vh',
    color: '#eee',
    marginLeft: '10px',
  },
  contentPresentation: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin: '5vh',
    color: '#FFF',
  },
  presentation: {
    background: '#1da1f2',
    height: '100vh',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  begin: {
    background: '#10171e',
    height: '100vh',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  contents: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: '15px',
  },
  appBar: {
    position: 'relative',
  },
  logo: {
    width: '200px',
    bottom: '30px',
  },
  socialButton: {
    width: '300px',
    fontWeight: 'bold',
    height: '44px',
    display: 'flex',
    position: 'relative',
    top: '20px',
    justifyContent: 'center',
  },

  '@media (min-width: 750px)': {
    container: {
      flexDirection: 'row',
    },
    presentation: {
      width: '50%',
    },
    begin: {
      width: '50%',
    },
    socialButton: {
      width: '370px',
    },
  },
};

export const Login = withStyles(styles)(
  withRouter(({ classes, history }) => {
    const twitterLogin = async () => {
      await Meteor.loginWithTwitter(
        {
          requestPermissions: ['user', 'user_timeline'],
        },
        error => {
          if (error) {
            return error;
          }
          history.push(RoutePaths.ROOT);
        }
      );
    };

    if (Meteor.userId()) {
      return <Redirect to={RoutePaths.ROOT} />;
    }

    return (
      <div className={classes.container}>
        <div className={classes.presentation}>
          <div className={classes.content}>
            <div className={classes.contentPresentation}>
              <Face fontSize="large" />
              <Typography className={classes.textPresention}>
                <FormattedMessage id="index_seeWhat" />
              </Typography>
            </div>
            <div className={classes.contentPresentation}>
              <HourglassEmpty fontSize="large" />
              <Typography className={classes.textPresention}>
                <FormattedMessage id="index_timeFirst" />
              </Typography>
            </div>
            <div className={classes.contentPresentation}>
              <FavoriteBorder fontSize="large" />
              <Typography className={classes.textPresention}>
                <FormattedMessage id="index_followRelevant" />
              </Typography>
            </div>
          </div>
        </div>

        <div className={classes.begin}>
          <Typography className={classes.topText}>
            <FormattedMessage id="index_welcome" />
          </Typography>

          <CardMedia
            className={classes.logo}
            component="img"
            alt="logo"
            image="logo.png"
            title="Titom"
          />

          <Typography className={classes.beginText}>
            <FormattedMessage id="index_slogan" />
          </Typography>

          <TwitterLoginButton
            text={<FormattedMessage id="index_login" />}
            className={classes.socialButton}
            onClick={twitterLogin}
          />
        </div>
      </div>
    );
  })
);
