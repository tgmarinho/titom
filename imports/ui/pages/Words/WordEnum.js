import { EnumExtended } from '../../../infra/EnumExtends';
// TODO: internacionalizar
class WordsEnum extends EnumExtended {}
WordsEnum.initEnum('Words', {
  DENIED: {
    find: 'findDeniedWords',
    add: 'addDeniedWord',
    delete: 'removeDeniedWord',
    deleteAll: 'removeAllDeniedWords',
    placeHolder: 'Palavra proíbida',
    title: 'menu_denyWords',
  },
  RELEVANT: {
    name: 'RELEVANT',
    find: 'findRelevantWords',
    add: 'addRelevantWord',
    delete: 'removeRelevantWord',
    deleteAll: 'removeAllRelevantWords',
    placeHolder: 'Palavra aceita',
    title: 'menu_relevantWords',
  },
});

export { WordsEnum };
