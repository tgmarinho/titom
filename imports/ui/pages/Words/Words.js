import React, { Fragment, useState, useEffect, useRef } from 'react';
import { default as AddIcon } from '@material-ui/icons/Add';
import { default as DeleteIcon } from '@material-ui/icons/Delete';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withStyles,
} from '@material-ui/core';
import classNames from 'classnames';
import { SnackbarProvider, withSnackbar } from 'notistack';
import PropTypes from 'prop-types';

import { methodCall } from '../../methods';
import { updateAppTitle } from '../../components/uis';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  topPaper: {
    padding: 5,
    width: '100%',
    display: 'flex',
  },
  divider: {
    margin: theme.spacing.unit,
    width: '100%',
  },
  words: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  grow: {
    flexGrow: 1,
  },
  wordChip: {
    margin: theme.spacing.unit,
    flexGrow: 1,
  },
});

const WordsContainer = props => {
  const { classes, WordEnum, enqueueSnackbar } = props;

  const inputRef = useRef(null);
  const [data, setData] = useState(null);
  const [word, setWord] = useState('');
  const [openDialog, setOpenDialog] = useState(false);

  useEffect(() => {
    methodCall(WordEnum.find)
      .then(words =>
        setData({
          words: words.map(word => ({ word: word.word, weight: word.weight })),
        })
      )
      .catch(err => {
        console.error(err);
      });
    // inputRef.current.focus();
  }, []);

  if (!data) {
    return <CircularProgress />;
  }

  const showSnackbar = (message, variant) => {
    // variant could be success, error, warning or info
    enqueueSnackbar(message, { variant });
  };

  const addWord = () => {
    if (!word) {
      showSnackbar('Informe a palavra', 'error');
      return;
    }
    if (
      data.words.map(w => w.word.toLowerCase()).includes(word.toLowerCase())
    ) {
      showSnackbar(`Palavra (${word}) já adicionada`, 'error');
      return;
    }

    methodCall(WordEnum.add, word)
      .then(id => {
        data.words.push({ word, weight: 1 });
        setData({ words: data.words });
        setWord('');
      })
      .catch(err => {
        setWord('');
        console.error(err);
      });
  };

  const deleteWord = word => {
    if (!word || !data.words.includes(word)) {
      return;
    }

    methodCall(WordEnum.delete, word)
      .then(() => {
        data.words.splice(data.words.indexOf(word), 1);
        setData({ words: data.words });
      })
      .catch(err => {
        console.error(err);
      });
  };

  const removeAll = () => {
    handleDialogClose();

    methodCall(WordEnum.deleteAll)
      .then(() => {
        setData({ words: [] });
        showSnackbar('Todos palavras foram excluídas', 'success');
      })
      .catch(err => {
        showSnackbar('Ops! Falha ao Excluir', 'error', err);
      });
  };

  const handleDialogOpen = () => {
    if (data.length) {
      return;
    }
    setOpenDialog(true);
  };

  const handleDialogClose = () => {
    setOpenDialog(false);
  };

  return (
    <Fragment>
      {updateAppTitle(<FormattedMessage id={WordEnum.title} />)}
      <div className={classNames(classes.container)}>
        <Paper elevation={1} className={classNames(classes.topPaper)}>
          <InputBase
            placeholder={WordEnum.placeHolder}
            className={classNames(classes.grow)}
            value={word}
            inputRef={inputRef}
            onChange={event => setWord(event.target.value)}
          />
          <IconButton aria-label="Adicionar" onClick={addWord}>
            <AddIcon />
          </IconButton>
        </Paper>

        <Divider className={classNames(classes.topPaper)} />

        <div className={classNames(classes.container)}>
          {data.words.map((word, index) => (
            <Chip
              className={classNames(classes.wordChip)}
              key={index}
              label={`${word.word} (${word.weight})`}
              onDelete={() => deleteWord(word)}
            />
          ))}
        </div>

        <Divider className={classNames(classes.topPaper)} />

        <Button
          variant="contained"
          color="secondary"
          className={classNames(classes.grow)}
          onClick={handleDialogOpen}
        >
          Apagar Todas
          <DeleteIcon />
        </Button>
      </div>
      <Dialog
        open={openDialog}
        onClose={handleDialogClose}
        aria-labelledby="title-dialog"
        aria-describedby="description-dialog"
      >
        <DialogTitle>{'Você realmente quer apagar tudo?'}</DialogTitle>
        <DialogContent>
          <DialogContentText>Isso vai deletar tudoooo!</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose} color="primary">
            Não!
          </Button>
          <Button onClick={removeAll} color="secondary" autoFocus>
            Concordo
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
};

WordsContainer.propTypes = {
  classes: PropTypes.object.isRequired,
  enqueueSnackbar: PropTypes.func.isRequired,
};

const MyApp = withSnackbar(withStyles(styles)(WordsContainer));

function Words(props) {
  return (
    <SnackbarProvider maxSnack={3}>
      <MyApp {...props} />
    </SnackbarProvider>
  );
}

export default Words;
