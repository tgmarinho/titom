import React, { Fragment, useState, useEffect } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FormattedMessage } from 'react-intl';
import { Typography } from '@material-ui/core';
// import { default as blue } from '@material-ui/core/colors/blue';
import { methodCall } from '../methods';
import { Posts } from '../components/Posts';
import { Loading } from '../components/Loading';
import { updateAppTitle } from '../components/uis';

const ONE_MINUTE = 10000;

export const Timeline = () => {
  // TODO guardar os tweets no Redux para o usuário ter acesso aos twwets quando mudar de tela e voltar aq manter os tweets
  const [posts, setPosts] = useState([]);
  // TODO fazer loading ser genérico e passsar vir prop para os compentes utulizarem useLoading por exemplo
  const [loading, setLoading] = useState(false);
  const [skip, setSkip] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [newsUpdate, setNewsUpdate] = useState(0);

  // https://overreacted.io/making-setinterval-declarative-with-react-hooks/
  const hasMoreUpdates = async () => {
    setInterval(() => {
      const createdAt = posts[0] && posts[0].created_at;
      if (createdAt) {
        const news = methodCall('lastPostReceived', createdAt);
        news
          .then(count => {
            setNewsUpdate(count);
          })
          .catch(err => {
            console.error(err);
            setNewsUpdate(0);
          });
      }
    }, ONE_MINUTE);
  };

  useEffect(() => {
    setLoading(true);
    window.document.title = newsUpdate > 0 ? `(${newsUpdate}) Titom` : 'Titom';
    const postsFromDB = methodCall('findTweets', skip);
    postsFromDB
      .then(postsBd => {
        setPosts(postsBd);
        setLoading(false);
      })
      .catch(err => {
        console.error(err);
        setLoading(false);
      });
  }, []);

  const hasMoreFunc = () => {
    const countPostsBD = methodCall('countPosts');
    countPostsBD
      .then(count => {
        if (count > posts.length) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      })
      .catch(err => {
        console.error(err);
        setHasMore(false);
      });
  };

  const fetchMore = () => {
    hasMoreFunc();
    setSkip(prevSkip => {
      const newSkip = prevSkip + 5;
      const postsFromDB = methodCall('findTweets', newSkip);
      postsFromDB
        .then(postsBd => {
          setPosts(posts.concat(postsBd));
        })
        .catch(err => {
          console.error(err);
        });

      return newSkip;
    });
  };

  const getPosts = () => {
    setLoading(true);
    setNewsUpdate(0);
    // clearInterval(interval);
    const newDatas = methodCall('getNewTweetsAndSave');
    newDatas
      .then(data => {
        setPosts(data || []);
        setLoading(false);
      })
      .catch(err => {
        console.error(err);
        setLoading(false);
      });
  };

  hasMoreUpdates();
  return (
    <>
      {updateAppTitle(<FormattedMessage id="menu_timeline" />)}
      {loading ? (
        <Loading />
      ) : (
        <Fragment>
          {/* <div>
      <Fab
        className={classNames(classes.fab, classes.fabBlue)}
        onClick={() => getPosts()}
        color="primary"
      >
        {newsUpdate > 0 ? `+${newsUpdate}` : <Refresh />}
      </Fab>
    </div> */}

          <InfiniteScroll
            dataLength={posts.length}
            next={fetchMore}
            hasMore={hasMore}
            loader={<Loading style={{ textAlign: 'center' }} />}
            endMessage={
              <Typography style={{ textAlign: 'center' }}>
                <FormattedMessage id="timeline_all_checked" />
              </Typography>
            }
          >
            <div>
              <Posts posts={posts} />
            </div>
          </InfiniteScroll>
        </Fragment>
      )}
    </>
  );
};
