import React from 'react';
import { addLocaleData, IntlProvider } from 'react-intl';
import { TunnelProvider } from 'react-tunnels';

import en from 'react-intl/locale-data/en';
import pt from 'react-intl/locale-data/pt';
import { flattenMessages } from '/imports/i18n/util';
import messages from '/imports/i18n/messages';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { App } from '/imports/ui/App';
import { BrowserRouter } from 'react-router-dom';

addLocaleData([...en, ...pt]);

let locale = window.navigator.language;
if (!messages[locale]) {
  locale = 'en-US';
}

// import '../imports/api/userMethods';

Meteor.startup(() => {
  render(
    <IntlProvider locale={locale} messages={flattenMessages(messages[locale])}>
      <TunnelProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </TunnelProvider>
    </IntlProvider>,
    document.getElementById('react-target')
  );
});
